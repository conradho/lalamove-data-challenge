import os
import time
import logging
import random

from kafka import KafkaProducer

from generator import Generator

# Environment variables
TOPIC = os.environ.get("TOPIC")
assert TOPIC, "Topic is not set"
SEED = int(os.environ.get("SEED", 100))
RECORDS = int(os.environ.get("RECORDS", 10000))
START_TIME = int(os.environ.get("START_TIME", 1514764800))
DRIVERS = int(os.environ.get("DRIVERS", 100))
LOG_LEVEL = os.environ.get("LOG_LEVEL", "WARNING")

random.seed(SEED)

# Initialize logger
logging.basicConfig(
    level=LOG_LEVEL,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger("Producer")

def main():
    start_time = time.time()

    kafka_producer = KafkaProducer(
            bootstrap_servers=['kafka:9092'],
            api_version=(0, 10))

    generator = Generator(
        drivers=DRIVERS, start_time=START_TIME, records=RECORDS)

    for message in generator.start():
        publish_message(kafka_producer, message)

    logger.info("Producer finished in {}s".format(round(time.time() - start_time, 2)))

def publish_message(producer, value):
    value_bytes = bytes(value, encoding='utf-8')
    producer.send(TOPIC, value=value_bytes)
    producer.flush()

if __name__ == "__main__":
    main()