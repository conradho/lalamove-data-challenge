# How to run
## Requirements
1. Docker
2. docker-compose

## Steps
1. Clone the project
```
git clone https://gitlab.com/conradho/lalamove-data-challenge.git
cd lalamove-data-challenge
```

2. Build all images
```
docker-compose build
```

3. Review environment variables in docker-compose.yaml (Optional)

4. Start all services
```
docker-compose up
```

5. Wait according to number of records being sent

6. View result
```
cat outputs/latest.txt
```


# Overview
This task involves a large amount of noisy streaming data with some degree of out of order.

# Design

## Kafka
A single node Kafka cluster is used.

## Producer
Producer is responsibe for generating messages and sending them to Kafka topic. It uses kafka-python to handle the communication between the application and Kafka. It exists after the required amount of messages are pushed to the Kafka topic.

## Consumer
Consumer is repsonible for processing the raw data stream and generating the final result text file. It uses Faust to handle data stream.

### Stream Processor
Python faust is used because it is easy to use and it supports asyncio which allows multiple stream processors running in same process. Faust only require Kafka which makes this project much more easier to setup. There are 3 stream processors(agent) to form the pipeline in consumer. For simplicity they are put in same file(process) but they can be seperated in production.

#### Raw data processor
As the raw data sent by producer is quite noisy, preprocessing is needed to make sure each message has the same schema. It filters out all the unnecessary fields so that the downstream data is consistent and small. Processed messages are sent to grouping processor.

#### Grouping processor
It uses Faust table and windowing to group multiple messages from same driver within the same time frame together(60 mins). When the window ends, grouped messages(unique drivers) are sent to output processor for final counting.

#### Output processor
It is very similar to grouping processor except it use single key in the table. It groups all unique drivers within same time frame so that we can collect the total number of drivers when the window is closed. Counts per mintue is written to the result file.

# Fault tolerance
Kafka offers fault tolerance by making replicas of partitions. </br>
For the consumers Faust persist data in the table to Kafka so that table data can be recovered during failure.

# Scalability
We can scale the Kafka cluster by adding more brokers in production.</br>
For the consumers the loading is distributed as number of partitions.

# Limitations
1. Output processor is not scalable
Output processor involves writting data to a single file so only 1 worker is allowed.

# References
1. [How To Install Apache Kafka on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-install-apache-kafka-on-ubuntu-18-04)

2. [Faust examples](https://github.com/robinhood/faust/tree/master/examples)

3. [wait-for-it.sh](https://github.com/vishnubob/wait-for-it)