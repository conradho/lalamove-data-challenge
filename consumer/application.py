import os
import time
import logging
import argparse
import asyncio
from datetime import datetime, timedelta

import faust

# Environment variables
RAW_DATA_TOPIC = os.environ.get("RAW_DATA_TOPIC")
assert RAW_DATA_TOPIC, "Raw data topic is not set"
WINDOW = int(os.environ.get("WINDOW", 60))
WINDOW_EXPIRES = int(os.environ.get("WINDOW_EXPIRES", 5))

# Constants
PROCESSED_DATA_TOPIC =  "PROCESSED_DATA"
OUTPUT_DATA_TOPIC = "OUTPUT_DATA"
GROUP_TABLE_NAME = "GROUP"
COUNT_TABLE_NAME = "COUNT"

class MessageModel(faust.Record):
    driver_id: str
    timestamp: datetime
    on_duty: bool

app = faust.App('Consumer', broker="kafka://kafka:9092", version=1, topic_partitions=3)

# Define topics
raw = app.topic(RAW_DATA_TOPIC, value_type=bytes)
processed = app.topic(PROCESSED_DATA_TOPIC, value_type=MessageModel)
output = app.topic(OUTPUT_DATA_TOPIC, value_type=MessageModel)

file_path = "/output/latest.txt"

def group_window_processor(key, messages):
    timestamp = key[1][0]
    is_online = messages[-1].on_duty
    asyncio.create_task(output.send(value=MessageModel(driver_id=key[0].decode(), timestamp=timestamp, on_duty=is_online)))

def count_window_processor(key, drivers):
    timestamp = key[1][0]
    on_duty_count = len([driver for driver in drivers if driver])
    datetime_obj = datetime.fromtimestamp(timestamp)
    datetime_str = datetime_obj.strftime("%Y-%m-%d %H:%M:%S")
    with open(file_path, "a") as file_obj:
        file_obj.write(f"{datetime_str}\t{on_duty_count}\t{len(drivers)}\n")

group_table = (
    app.Table(
        GROUP_TABLE_NAME,
        default=list,
        partitions=1,
        on_window_close=group_window_processor,
    )
    .tumbling(WINDOW, expires=timedelta(seconds=WINDOW_EXPIRES))
    .relative_to_field(MessageModel.timestamp)
)

count_table = (
    app.Table(
        COUNT_TABLE_NAME,
        default=list,
        partitions=1,
        on_window_close=count_window_processor,
    )
    .tumbling(WINDOW, expires=timedelta(seconds=WINDOW_EXPIRES))
    .relative_to_field(MessageModel.timestamp)
)

@app.agent(raw)
async def raw_agent(stream):
    async for value in stream:
        asyncio.create_task(processed.send(key=str(value["driver_id"]), value=MessageModel(driver_id=value["driver_id"], timestamp=int(value["timestamp"]), on_duty=bool(value["on_duty"]))))


@app.agent(processed)
async def group_agent(stream):
    async for key, value in stream.items():
        messages = group_table[key].value()
        messages.append(value)
        group_table[key] = messages

@app.agent(output)
async def count_agent(stream):
    async for value in stream:
        drviers = count_table["count"].value()
        drviers.append(value.on_duty)
        count_table["count"] = drviers

if __name__ == "__main__":
    with open(file_path, "w") as file_obj:
        file_obj.write("Time\tOnline Drivers\tAvailable Drivers\n")
    app.main()